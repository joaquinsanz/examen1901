<?php


class App
{

    public function __construct()
    {
        session_start();
    }

    public function vista()
    {
        require("fase1.php");
    }

    public function datosPersonales()
    {
        if( $_POST["submit"] )
        {
            foreach($_POST as $valor => $valor)
            {
                $_SESSION['registro'][$valor] = $valor;
            }
        }
        header("Location: /registro/fase2.php");

    }
    public function modulos()
    {
        if($_POST["submit"] )
        {
            foreach($_POST as $valor => $valor)
            {
                $_SESSION['productos'][$valor]=$valor;
             } 
        }
        header("Location: /registro/fase4.php");

    }
    public function delete()
    {
        
        unset($_SESSION['modulos']);
        unset($_SESSION['registro']);
        header ('Location: /registro/index.php');
        
    }
}

$app = new App();
if (isset($_GET['method'])) 
{
    $method = $_GET['method'];
} else {
    $method = 'vista';
}
if (method_exists($app, $method)) 
{
    $app->$method();
} else 
{
    exit('no encontrado');
}
